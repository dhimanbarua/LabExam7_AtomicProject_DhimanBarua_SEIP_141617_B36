<?php
namespace App\Hobbies;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class Hobbies extends DB
{
    public $id="";

    public $name="";

    public $hobbies="";

    public function __construct()
    {
        parent::__construct();

    }

    public function setData($data=NULL)
    {
        if(array_key_exists('id',$data))
        {
            $this->user_id = $data['id'];
        }
        if(array_key_exists('name',$data))
        {
            $this->name= $data['name'];
        }
        if(array_key_exists('hobbies',$data))
        {
            $this->hobbies = $data['hobbies'];
        }
    }
    public function store()
    {
        $arrData = array($this->name,$this->hobbies);
        $conn=$this->DBH;
        $STH=$conn->prepare("INSERT INTO hobbies(name,hobbies) VALUES(?,?) ");
        $STH->execute($arrData);

        if($STH)
        {
            Message::message("<div id='msg'><h3 align='center'>[User Name: $this->name],[Hobbie:$this->hobbies]
                    <br>Data Has been Inserted Successfully!!!!!!</h3></div> ");
        }
        else
        {
            Message::message("<div id='msg'><h3 align='center'>>[User Name: $this->name],[Hobbie:$this->hobbies]
                    <br>Data Has Not been Inserted Successfully!!!!!!</h3> </div>");
        }
        Utility::redirect('create.php');


    }


}