<?php
namespace App\SummaryOfOrganization;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class SummaryOfOrganization extends DB
{
    public $id="";

    public $name="";

    public $summaryOfOrganization="";

    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data=NULL)
    {
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data))
        {
            $this->name= $data['name'];
        }
        if(array_key_exists('summaryOfOrganization',$data))
        {
            $this->summaryOfOrganization = $data['summaryOfOrganization'];
        }
    }
    public function store()
    {
        $arrData = array($this->name,$this->summaryOfOrganization);
        $conn=$this->DBH;
        $STH=$conn->prepare("INSERT INTO summary_of_organization(name,summaryOfOrganization) VALUES(?,?) ");
        $STH->execute($arrData);

        if($STH)
        {
            Message::message("<div id='msg'><h3 align='center'>[Organization_Name: $this->name],
            [Summary Of Organization:$this->summaryOfOrganization]
                    <br>Data Has been Inserted Successfully!!!!!!</h3></div> ");
        }
        else
        {
            Message::message("<div id='msg'><h3 align='center'>[Organization_Name: $this->name],
            [Summary Of Organization:$this->summaryOfOrganization]
              <br>Data Has Not been Inserted Successfully!!!!!!</h3></div> ");
        }
        Utility::redirect('create.php');

    }

}