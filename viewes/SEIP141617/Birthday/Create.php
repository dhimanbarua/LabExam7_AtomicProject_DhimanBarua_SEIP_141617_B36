<html>
<head>
    <title>Birthday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row-fluid user-row ">
                        <div class="book_title"><center><h2>Add Birth Date</h2></center></div>
                        <div class="img_icon"><img src="../../../Resources/Images/bd.jpg" class="img-responsive" width="150px" height="150px" alt="Conxole Admin"/></div>
                    </div>
                </div>
                <div class="panel-body">
                    <form action="store.php" method="post" accept-charset="UTF-8" role="form" class="form-signin">
                        <fieldset>
                            <label class="panel-login">
                                <div class="login_result"></div>
                            </label>

                            <div>
                                <?php

                                require_once("../../../vendor/autoload.php");
                                use App\Message\Message;

                                if(!isset($_SESSION)) session_start();
                                echo Message::message();
                                ?>
                            </div>

                            <label>User Name:</label><input class="form-control" name="name" placeholder="enter your name"  type="text">
                            <br>
                            <label>Enter Your Birthday:</label>
                            <br>
                            <input name="birthday"  type="date" value="<?php echo date('Y-m-d'); ?>">
                            <br>
                            <br>
                            <input class="btn btn-lg btn-success " type="submit" value="Add">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>

</html>