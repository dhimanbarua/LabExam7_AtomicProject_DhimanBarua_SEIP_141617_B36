-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 12, 2016 at 09:28 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomic_project_b36`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) NOT NULL,
  `birthday` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birthday`) VALUES
(1, 'Muhammad Iqbal hossain', '1993-12-16'),
(2, 'Muhammad Sakib', '1997-10-12'),
(3, 'Avijit Barua', '2016-11-12');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(56) NOT NULL,
  `book_title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `author_name`, `book_title`) VALUES
(1, 'Humayun Ahmed', 'Himu'),
(2, 'Jafar Iqbal', 'NatBoltu'),
(3, 'iqbal', 'Himuuuuu'),
(4, 'jafariqbal1', 'natboltub'),
(5, 'sakib', 'kahini'),
(6, 'kdlhfsahda', 'ahsahdsdh'),
(7, '5685468746879', 'fsfaf'),
(8, '646748', 'alkdsahd'),
(9, '54687687567', 'srewrewrw``'),
(10, '74558377415428', 'gdgdsg'),
(11, 'Humayun', 'Himu');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `city` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`) VALUES
(1, 'Muhammad Iqbal hossain', 'Chittagong'),
(2, 'Muhammad sakib', 'Dhaka'),
(3, 'Mishu', 'Dhaka');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) NOT NULL,
  `email` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`) VALUES
(1, 'Muhammad Iqbal hossain', 'silentiq16@gmail.com'),
(2, 'Muhammad sakib', 'iqbal_2020@yahoo.com'),
(3, '', 'dhimanctg007@yahoo.com'),
(4, 'Avijit Barua', 'dhiman.hb@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) NOT NULL,
  `gender` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`) VALUES
(1, 'Muhammad Iqbal hossain', 'Male'),
(2, 'Mrs. Umme Habiba ', 'Female'),
(3, '', 'Male'),
(4, 'Dhiman ', 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) NOT NULL,
  `hobbies` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`) VALUES
(1, 'Muhammad Iqbal hossain', 'Gardening'),
(2, 'Muhammad sakib', 'Angling'),
(3, 'Dhiman ', 'r'),
(4, 'Mishu', 'd'),
(5, 'Thusar', 'swimming'),
(6, 'Mishu', 'reading,dancing'),
(7, 'tttt', 'swimming,running');

-- --------------------------------------------------------

--
-- Table structure for table `profilepic`
--

CREATE TABLE IF NOT EXISTS `profilepic` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) NOT NULL,
  `profilePicture` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `profilepic`
--

INSERT INTO `profilepic` (`id`, `name`, `profilePicture`) VALUES
(1, 'Muhammad Iqbal hossain', 'C//desktop/iqbal'),
(2, 'Muhammad sakib', 'C//desktop/sakib'),
(3, '', ''),
(4, '', ''),
(5, '', ''),
(6, 'eee', ''),
(7, 'dddd', ''),
(8, 'Sumoy', '1478946157booktitle.jpg'),
(9, 'tttt', '1478949618booktitle.jpg'),
(10, 'Avijit Barua', '1478978222butler-inn20161010003300.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE IF NOT EXISTS `summary_of_organization` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) NOT NULL,
  `summaryOfOrganization` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `name`, `summaryOfOrganization`) VALUES
(1, 'Muhammad Iqbal hossain', 'BITM'),
(2, 'Muhammad Sakib', 'Google'),
(3, 'WordPress IT', 'WordPress theme developer ');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
